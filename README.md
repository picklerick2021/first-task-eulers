[![pipeline status](https://gitlab.com/picklerick2021/Munchkin/badges/main/pipeline.svg)](https://gitlab.com/picklerick2021/Munchkin/-/commits/main)
&nbsp;&nbsp;&nbsp;
[![coverage report](https://gitlab.com/picklerick2021/Munchkin/badges/main/coverage.svg)](https://gitlab.com/picklerick2021/Munchkin/-/commits/main)
# Pair Finder Framework

This repository contains a pair finder framework, which is created while participating at the workshop [Do Research Like a Munchkin](https://the-munchkins.gitlab.io/do-research-like-a-munchkin/).

The Pair Finder Framework allows to run several pair finding algorithms using a common command and results interface.

# Installation

First clone git repository. We strongly recommend to create a virtual python environment (venv, conda, etc). Therein, as a user all packages in `requirements.txt` need to be installed, e.g. by typing:
```commandline
pip install -r requirements.txt
```

Developers additionally need:
```commandline
pip install -r requirements-dev.txt
```

# Quickstart

The framework can be run from the commandline (with actived python environment) and is accepting commandline arguments as described by:  
```commandline
python run.py --help
```

Also a Jupyter notebook can be used to run the framework. An example notebook will be generated in the subfolder 'reports' by running:
```commandline
python run.py --report
```

It generates the following python code:
```python
# About PairFinderFramework: https://gitlab.com/picklerick2021/Munchkin/-/tree/main
# Read the docs: https://picklerick2021.gitlab.io/Munchkin/run_main/
%matplotlib widget
import sys
sys.path.append('..')
from options import Options
from run import *
import report
import matplotlib.pyplot as plt

plt.rcParams["figure.figsize"] = 6, 6
op = Options(dims=1, size=100, seed=1642703247, max_runtime=29900, finder='Baseline_pair_finding_method', custom=None, report=None, apns=None)

fp = main(op)
```
Upon running it shows results like this of the permutation finder:
![](permutation_finder.gif)

# Documentation / Finders

Please read the docs https://picklerick2021.gitlab.io/Munchkin/.

# Software architecture

Please continue reading in [arc42](arc42.md) standard document.
