from main import *


def get_pairs(points: list[Object], dist_init: float = 1e99) -> dict[int, list[Pair]]:
    pairs: dict[int, list[Pair]] = {}
    out: list[int] = []

    cpoint: int
    point: Point
    for cpoint, point in enumerate(points):
        if cpoint not in out:
            pairs[cpoint] = Pair(cpoint, dist_init)
            for cother in range(cpoint + 1, len(points)):
            # for cother in range(len(points)):
                if cother not in out and cother != cpoint:
                # if cother != cpoint:
                    pairs[cpoint].check(cother, point.distance_to_object(points[cother]))
            out.extend(pairs[cpoint].members)
            # out.extend([pairs[cpoint].members[0]])
            # out.append(cpoint)
    return pairs


def get_pairs_non_unique(points:list[Object], dist_init:float = 1e99) -> dict[int, list[Pair]]:
    pairs: dict[int, list[Pair]] = {}
    out: list[int] = []

    cpoint: int
    point: Point
    for cpoint, point in enumerate(points):
        # if cpoint not in out:
            pairs[cpoint] = Pair(cpoint, dist_init)
            # for cother in range(cpoint + 1, len(points)):
            for cother in range(len(points)):
                # if cother not in out and cother != cpoint:
                if cother != cpoint:
                    pairs[cpoint].check(cother, point.distance_to_object(points[cother]))
            # out.extend(pairs[cpoint].members)
            # out.extend([pairs[cpoint].members[0]])
            # out.append(cpoint)
    return pairs


def cache_key(a:int, b:int) -> str:
    if a < b:
        k, l = a, b
    else:
        k, l = b, a
    return f'{k}-{l}'


def get_pairs_non_unique_cached(points:list[Object], dist_init:float = 1e99) -> dict[int, list[Pair]]:
    pairs: dict[int, list[Pair]] = {}
    out: list[int] = []
    cache: dict[str, float] = {}

    cpoint: int
    point: Point
    for cpoint, point in enumerate(points):
        # if cpoint not in out:
            pairs[cpoint] = Pair(cpoint, dist_init)
            # for cother in range(cpoint + 1, len(points)):
            for cother in range(len(points)):
                # if cother not in out and cother != cpoint:
                if cother != cpoint:
                    key = cache_key(cpoint, cother)
                    if key not in cache:
                        dto = cache[key] = point.distance_to_object(points[cother])
                    else:
                        dto = cache[key]
                    pairs[cpoint].check(cother, dto)
            # out.extend(pairs[cpoint].members)
            # out.extend([pairs[cpoint].members[0]])
            # out.append(cpoint)
    return pairs


def test():
    points: list[Single] = [Single(-10), Single(-1), Single(-2), Single(1)]
    pairs: list[Pair] = get_double_pairs(points)
    print_pairs(points, pairs, True)
