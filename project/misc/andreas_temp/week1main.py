#!/bin/env python
"""
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Find the sum of all the multiples of 3 or 5 below 1000.
"""
import math
from timeit import default_timer as timer
import time

def make_list(maxval: int = 1000, divs: list[int] = [3, 5]) -> list[int]:
    """
    ret: list(int) = []
    for val in range(1, maxval):
        for div in divs:
            if val % div == 0:
                ret.append(val)
    return ret
    """
    return [val for val in range(1, maxval) if any(val % div == 0 for div in divs)]


def make_sum(maxval: int = 1000, divs: list[int] = [3, 5]) -> list[int]:
    """
    ret: list(int) = []
    for val in range(1, maxval):
        for div in divs:
            if val % div == 0:
                ret.append(val)
    return ret
    """
    return sum([val for val in range(1, maxval) if any(val % div == 0 for div in divs)])


def format_pretty_list(l: list[int]) -> str:
    ret = ""
    if len(l) == 0:
        return ""
    elif len(l) >= 1:
        ilen:int = math.ceil(math.log10(l[-1]+1))
    for index, item in enumerate(l[:-1], start=1):
        ret = ret + f"{item:{ilen}d}, "
        if index % 10 == 0:
            ret = ret + '\n'
    ret = ret + f"{l[-1]}"
    return ret


def main(maxval: int = 1000, divs: list[int] = [3, 5]):
    vals: list[int] = make_list(maxval, divs)
    #print(", ".join(map(str, vals)))
    print(format_pretty_list(vals))
    print("\nSum is {sum}".format(sum=sum(vals)))


if __name__ == "__main__":
    #timeit.timeit(main, number=10000)
    #main(maxval=10000000)

    limit = 10000000

    a = time.time()
    print(make_sum(limit))
    b = time.time()
    print("Time: {}".format(b - a))

    #timeit.timeit(make_sum, number=1000)

    #start = timer()
    #print(make_sum(10000000))
    #end = timer()
    #print(end - start)

    a = time.time()
    [make_sum(int(limit/1000)) for i in range(1000)]
    b = time.time()
    print("Time: {}".format(b - a))
