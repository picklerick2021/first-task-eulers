# About

## {{ config.site_name }}

The {{ config.site_name }} allows to run several pair finding algorithms using a common command and results interface.

## Team PickleRick

In alphabetical order:

- Shubhayu Das

- Jeroen van Dijk

- Andreas Houben

- Noah Nachtigall
