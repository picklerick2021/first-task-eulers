from typing import Union, Callable

import matplotlib.figure
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib.collections import LineCollection
from mpl_toolkits.mplot3d.art3d import Line3DCollection
import numpy as np

# for Player
from matplotlib.animation import FuncAnimation
import mpl_toolkits.axes_grid1
import matplotlib.widgets


class Player(FuncAnimation):
    """
    Animation class derived from FuncAnimation.
    """
    def __init__(self, fig: matplotlib.figure.Figure, func, frames=None, init_func=None, fargs=None,
                 save_count=None, mini=0, maxi=100, pos=(0.125, 0.92), **kwargs):
        """
        Additional arguments see base class.
        Args:
            fig: Animation is drawn here.
            func: Function called for each frame.
            frames:
            init_func:
            fargs:
            save_count:
            mini:
            maxi:
            pos:
            **kwargs:
        """
        self.i = 0
        self.min = mini
        self.max = maxi
        self.runs = True
        self.forwards = True
        self.fig = fig
        self.func = func
        self.setup(pos)
        FuncAnimation.__init__(self, self.fig, self.update, frames=self.play(),
                               init_func=init_func, fargs=fargs,
                               save_count=save_count, **kwargs)

    def play(self):
        """
        Next frame.
        """
        while self.runs:
            self.i = self.i + self.forwards - (not self.forwards)
            if self.i > self.min and self.i < self.max:
                yield self.i
            else:
                self.stop()
                yield self.i

    def start(self):
        """
        Start playback.
        """
        self.runs = True
        self.event_source.start()

    def stop(self, event=None):
        """
        Stop playback.
        """
        self.runs = False
        self.event_source.stop()

    def forward(self, event=None):
        """
        Playback in forward direction.
        """
        self.forwards = True
        if self.i < self.max:
            self.start()

    def backward(self, event=None):
        """
        Playback in backward direction.
        """
        self.forwards = False
        if self.i > self.min:
            self.start()

    def oneforward(self, event=None):
        """
        One frame forward.
        """
        self.forwards = True
        self.onestep()

    def onebackward(self, event=None):
        """
        One frame backward.
        """
        self.forwards = False
        self.onestep()

    def onestep(self):
        """
        One frame further (in last direction).
        """
        if self.i > self.min and self.i < self.max:
            self.i = self.i + self.forwards - (not self.forwards)
        elif self.i == self.min and self.forwards:
            self.i += 1
        elif self.i == self.max and not self.forwards:
            self.i -= 1
        self.func(self.i)
        self.slider.set_val(self.i)
        self.fig.canvas.draw_idle()

    def setup(self, pos):
        """Setup drawing."""
        playerax = self.fig.add_axes([pos[0], pos[1], 0.64, 0.034])
        divider = mpl_toolkits.axes_grid1.make_axes_locatable(playerax)
        bax = divider.append_axes("right", size="80%", pad=0.05)
        sax = divider.append_axes("right", size="80%", pad=0.05)
        fax = divider.append_axes("right", size="80%", pad=0.05)
        ofax = divider.append_axes("right", size="100%", pad=0.05)
        sliderax = divider.append_axes("right", size="500%", pad=0.07)
        self.button_oneback = matplotlib.widgets.Button(playerax, label='$\u29CF$')
        self.button_back = matplotlib.widgets.Button(bax, label='$\u25C0$')
        self.button_stop = matplotlib.widgets.Button(sax, label='$\u25A0$')
        self.button_forward = matplotlib.widgets.Button(fax, label='$\u25B6$')
        self.button_oneforward = matplotlib.widgets.Button(ofax, label='$\u29D0$')
        self.button_oneback.on_clicked(self.onebackward)
        self.button_back.on_clicked(self.backward)
        self.button_stop.on_clicked(self.stop)
        self.button_forward.on_clicked(self.forward)
        self.button_oneforward.on_clicked(self.oneforward)
        self.slider = matplotlib.widgets.Slider(sliderax, '',
                                                self.min, self.max, valinit=self.i)
        self.slider.on_changed(self.set_pos)

    def set_pos(self, i):
        """Set frame position."""
        self.i = int(self.slider.val)
        self.func(self.i)

    def update(self, i):
        """Update slider."""
        self.slider.set_val(i)


def fig_add_subplot_for_population(fig: matplotlib.figure.Figure, pop: np.ndarray, *args, **kwargs) \
        -> Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D]:
    """
    Create subplot according to the dimension of the population.
    Args:
        fig: Draw to this figure.
        pop: Population.
        *args (tuple): Argument list to subplot.
        **kwargs (dict): Argument dict to subplot.

    Returns:
        Axes or Axes3D object.
    """
    if pop.shape[1] <= 2:
        ax = fig.add_subplot(*args, **kwargs)
    else:
        ax = fig.add_subplot(*args, projection='3d', **kwargs)
    return ax


def fig_init(fig: matplotlib.figure.Figure) -> matplotlib.figure.Figure:
    """
    Args:
        fig: Initialize this Figure.

    Returns:
        Same Figure.
    """
    fig.canvas.toolbar_visible = True
    fig.canvas.header_visible = False
    fig.canvas.footer_visible = False
    return fig


def plot_population(pop: np.ndarray, pairs: np.ndarray, info: str = None, title: str = None, windowtitle: str = None,
                    show: bool = True, fig: matplotlib.figure.Figure = None,
                    ax: Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D] = None) \
        -> (matplotlib.figure.Figure,
            Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D],
            Union[LineCollection, Line3DCollection]):
    """
    Plot population to Figure (or create a new one).
    Args:
        pop: Population as array of coordinates.
        pairs: Pair indexes.
        info: Suptitle of window.
        title: Plot title.
        windowtitle: Window caption.
        show:
        fig: Already existing Figure to plot on.
        ax: Already existing Axes to plot on.

    Returns:
        Figure, Axes | Axes3D, LineCollection | Line3DCollection
    """
    if pop.shape[1] > 3:
        print(f'Plotting of {pop.shape[1]} dimensions is not supported!')
        return False

    if title is None:
        title = 'Points'
    if windowtitle is None:
        windowtitle = 'Points'

    if fig is None:
        fig = plt.figure()
        fig_init(fig)
        fig.canvas.manager.set_window_title(windowtitle)
        fig.suptitle(info)
    if ax is None:
        ax = fig_add_subplot_for_population(fig, pop)

    ax.set_title(title)
    ax.set_xlabel('x')

    if pop.shape[1] == 1:
        ax.set_ylabel('x')
        pp = np.hstack((pop, pop))
        ax.scatter(pp[:, 0], pp[:, 1])
        lc = LineCollection(pp[pairs], colors='r')
        ax.add_collection(lc)
    elif pop.shape[1] == 2:
        ax.set_ylabel('y')
        ax.scatter(pop[:, 0], pop[:, 1])
        lc = LineCollection(pop[pairs], colors='r')
        ax.add_collection(lc)
    else:
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.scatter(pop[:, 0], pop[:, 1], pop[:, 2])
        lc = Line3DCollection(pop[pairs], colors='r')
        ax.add_collection(lc)
    if show:
        plt.show(block=False)
    return fig, ax, lc


def plot_pair_animation(pop: np.ndarray, history: np.ndarray, info: str = None, title: str = None,
                        windowtitle: str = None, show: bool = True, fig: matplotlib.figure.Figure = None,
                        ax: Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D] = None,
                        ani: Player = None) \
        -> (matplotlib.figure.Figure,
            Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D],
            Union[LineCollection, Line3DCollection],
            Union[Player, Callable]):
    """
    Plot animated population to Figure (or create a new one).
    Args:
        pop: Population as array of coordinates.
        history: Array of dsum and Pair indexes.
        info: Suptitle of window.
        title: Plot title.
        windowtitle: Window caption.
        show:
        fig: Already existing Figure to plot on.
        ax: Already existing Axes to plot on.
        ani: Already existing Player.

    Returns:
        Figure, Axes | Axes3D, LineCollection | Line3DCollection, Player | callable update function
    """
    def get_info(dsum: float) -> str:
        return f'Sum of all pair distances {dsum:.3f}'

    lc: LineCollection
    fig, ax, lc = plot_population(pop, history[0][1], info=get_info(history[0][0]), title=title,
                                  windowtitle=windowtitle, show=False, fig=fig, ax=ax)

    def update(i: int):
        if pop.shape[1] == 1:
            pp = np.hstack((pop, pop))
        else:
            pp = pop
        if 0 <= i < len(history):
            fig.suptitle(get_info(history[i][0]))
            lc.set_segments(pp[history[i][1]])

    if ani is None:
        ani = Player(fig, update, maxi=len(history), save_count=len(history)+1, pos=(0.125, 0.919), blit=False)
    else:
        ani = update

    if show:
        plt.show(block=False)
    return fig, ax, lc, ani


def plot_history_dsums(dsums: np.ndarray, info: str = None, title: str = None, windowtitle: str = None,
                       show: bool = True, fig: matplotlib.figure.Figure = None,
                       ax: Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D] = None) \
        -> (matplotlib.figure.Figure, matplotlib.pyplot.Axes, matplotlib.artist.Artist):
    """
    Plot history of distance summs to Figure (or create a new one).
    Args:
        dsums: Array of distance summs.
        info: Suptitle of window.
        title: Plot title.
        windowtitle: Window caption.
        show:
        fig: Already existing Figure to plot on.
        ax: Already existing Axes to plot on.

    Returns:
        Figure, Axes, Artist
    """
    if title is None:
        title = 'History of distance sums'
    if windowtitle is None:
        windowtitle = 'History of distance sums'

    if fig is None:
        fig = plt.figure()
        fig_init(fig)
        fig.canvas.manager.set_window_title(windowtitle)
        fig.suptitle(info)
    if ax is None:
        ax = fig.add_subplot()

    ax.set_title(title)
    ax.set_xlabel('point in history')
    ax.set_ylabel('distance sum')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    y = np.append(dsums, dsums[-1])
    x = np.append(np.arange(1, len(y)), 'end')
    artist = ax.step(x, y, where='post')
    # artist = ax.plot(x, y)
    ax.set_ylim(bottom=0)
    if show:
        plt.show(block=False)
    return fig, ax, artist


def plot_history_dsums_animation(history: np.ndarray, info: str = None, title: str = None, windowtitle: str = None,
                                 show: bool = True, fig: matplotlib.figure.Figure = None,
                                 ax: Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D] = None,
                                 ani: Player = None) \
        -> (matplotlib.figure.Figure, matplotlib.pyplot.Axes, Union[Player, Callable]):
    """
    Plot animated history of distance summs to Figure (or create a new one).
    Args:
        history: Array of distance summs.
        info: Suptitle of window.
        title: Plot title.
        windowtitle: Window caption.
        show:
        fig: Already existing Figure to plot on.
        ax: Already existing Axes to plot on.
        ani: Already existing Player.

    Returns:
        Figure, Axes | Axes3D, Player | callable update function
    """
    dsums = history[:, 0]
    y = np.append(dsums, dsums[-1])
    x = np.append(np.arange(1, len(y)), 'end')
    fig, ax, artist = plot_history_dsums(dsums, title=title, windowtitle=windowtitle, show=False, fig=fig, ax=ax)
    xl = ax.get_xlim()
    yl = ax.get_ylim()

    def update(i):
        if 0 <= i < len(y):
            ax.clear()
            ax.set_xlim(xl)
            ax.set_ylim(yl)
            plot_history_dsums(dsums[0:i+1], title=title, windowtitle=windowtitle, show=False, fig=fig, ax=ax)

    update(0)
    if ani is None:
        ani = Player(fig, update, maxi=len(history), save_count=len(history)+1, pos=(0.125, 0.919), blit=False)
    else:
        ani = update

    if show:
        plt.show(block=False)
    return fig, ax, ani


def plot_pairs_history(pop: np.ndarray, history: np.ndarray, dsums: np.ndarray,
                       info: str = None, windowtitle: str = None, show: bool = True,
                       fig: matplotlib.figure.Figure = None) \
        -> (matplotlib.figure.Figure,
            Union[matplotlib.pyplot.Axes, mpl_toolkits.mplot3d.axes3d.Axes3D],
            matplotlib.pyplot.Axes,
            Player):
    """
    Plot animated pairs and history of distance summs to Figure (or create a new one).
    Args:
        pop: Population as array of coordinates.
        history: Array of dsum and Pair indexes.
        dsums: Array of distance summs.
        info: Suptitle of window.
        windowtitle: Window caption.
        show:
        fig: Already existing Figure to plot on.

    Returns:
        Figure, Axes | Axes3D, Axes, Player
    """
    if fig is None:
        xw, yw = plt.rcParams["figure.figsize"]
        fig = plt.figure(figsize=(xw*2, yw))
        fig_init(fig)
        fig.canvas.manager.set_window_title(windowtitle)
        fig.suptitle(info)
    ax_pop = fig_add_subplot_for_population(fig, pop, 1, 2, 1)
    ax_hist = fig.add_subplot(1, 2, 2)
    ax_pop.set(adjustable='box')
    ax_hist.set(adjustable='box')
    # plot_population(pop=pop, pairs=pairs, info=info, show=False, fig=fig, ax=ax_pop)
    fig_ppa, ax_ppa, lc_ppa, ani_ppa = plot_pair_animation(pop=pop, history=history, info=info, show=False, fig=fig, ax=ax_pop, ani=True)
    # plot_history_dsums(dsums=dsums, info=info, show=False, fig=fig, ax=ax_hist)
    fig_hda, ax_hda, ani_hda = plot_history_dsums_animation(history=history, info=info, show=False, fig=fig, ax=ax_hist, ani=True)

    def update(i):
        ani_ppa(i)
        ani_hda(i)

    ani = Player(fig, update, maxi=len(history), save_count=len(history)+1, pos=(0.125, 0.919), blit=False)

    if show:
        mngr = plt.get_current_fig_manager()
        try:
            geom = mngr.window.geometry()
            x, y, dx, dy = geom.getRect()
            mngr.window.setGeometry(100, 100, dx, dy)
        except AttributeError:
            pass
        plt.show(block=False)
    return fig, ax_pop, ax_hist, ani


def plot_show():
    plt.show()
