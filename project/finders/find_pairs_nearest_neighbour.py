from find_pairs import FindPairs
from sklearn.neighbors import *
from sklearn.cluster import *


class NearestNeighbour:
    """
    Class using a nearest neighbour approach to find the best pairs with minimal summed distance
    """
    def __init__(self, find_pairs: 'FindPairs'):
        self.find_pairs = find_pairs

    def run(self) -> None:
        """
        Executes the nearest neighbour finder
        Returns:
            nothing
        """
        population = self.find_pairs.population

        kmeans = KMeans(n_clusters=int(len(population) / 2))
        kmeans.fit(population)
