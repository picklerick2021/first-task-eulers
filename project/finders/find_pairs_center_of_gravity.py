import numpy as np
from population import Population


def center_of_gravity(self, tzero=None, max_runtime=29900, *args, **kwargs):
    # 1.load particles with different coordinates
    pair_list = np.zeros([int(self.size / 2), 2], dtype=int)
    particles = self.population
    particle_locations = particles.copy()
    # 2.calculate the center of gravity
    center_of_gravity = particles.sum(axis=0) / self.size

    for pair in range(self.size // 2):
        # 3.determine particle with biggest distance from COG
        distance_to_center_of_gravity = np.zeros(int(particles.size / 2))
        for i, p in enumerate(particles):
            distance_to_center_of_gravity[i] = self.distance_function_1(p, center_of_gravity)
        index_particle_biggest_distance = np.where(
            distance_to_center_of_gravity == np.max(distance_to_center_of_gravity))
        true_index_biggest_distance = np.where(particle_locations == particles[index_particle_biggest_distance[0][0]])
        particles = np.delete(particles, index_particle_biggest_distance, axis=0)
        # 4.find nearest neighbor
        distance_to_particle = np.zeros(int(particles.size / 2))
        for i, p in enumerate(particles):
            distance_to_particle[i] = Population.distance_function_1(p, particle_locations[
                true_index_biggest_distance[0][0]])
        index_nearest_neighbor = np.where(distance_to_particle == np.min(distance_to_particle))
        true_index_nearest_neighbor = np.where(particle_locations == particles[index_nearest_neighbor])
        particles = np.delete(particles, index_nearest_neighbor, axis=0)
        # 5.safe both into pair list
        pair_list[pair] = [int(true_index_biggest_distance[0][0]), int(true_index_nearest_neighbor[0][0])]
    # 7.iterate over all particles
    self.pairs = pair_list
    self.add_to_history()