import sys
sys.path.append('../finders')

import pytest
from plotting import plot_history_dsums
import numpy as np
import matplotlib.figure
from PIL import Image
import os
from run import *
import matplotlib.pyplot as plt


def test_plot_history_dsums():
    fig, ax, artist = plot_history_dsums(np.random.random(10), None, None, None)

    fig_dir = "plots"
    fig_file = "test_plot_history_dsums.png"

    os.makedirs(fig_dir, exist_ok=True)
    fig_path = os.path.join(fig_dir, fig_file)

    fig.savefig(fig_path, format="png")
    im = np.reshape(np.array(Image.open(fig_path)), (480 * 640, 4))
    os.remove(fig_path)
    os.rmdir(fig_dir)

    assert isinstance(fig, matplotlib.figure.Figure)
    assert len(fig.canvas.manager.get_window_title()) > 0
    assert len(fig.axes[0].title._text) > 0
    assert len(fig.axes[0].xaxis.axis_name) > 0
    assert len(fig.axes[0].yaxis.axis_name) > 0
    assert np.sum([(k[2] > k[1]) and (k[2] > k[0]) for k in im]) > 0  # checks if there is blue in the plot


@pytest.mark.parametrize('dims',
                         [
                             1,
                             2,
                             3
                         ])
def test_report(dims):
    report_dir = "test_reports"
    report_file = "test_report.ipynb"
    os.makedirs(report_dir, exist_ok=True)
    report_path = os.path.join(report_dir, report_file)

    op = Options(dims=dims, size=4, seed=1, max_runtime=29900, finder=pair_finders[0], report=report_path, apns=None)

    if op.seed is not None:
        np.random.seed(op.seed)

    manager = FindPairManager(op)
    manager.create_report()

    assert os.path.exists(report_path)

    os.remove(report_path)
    os.rmdir(report_dir)
