import sys
sys.path.append('../finders')

from find_pairs import FindPairs
import numpy as np
from finders.find_pairs_cluster import Cluster


def test_cluster():
    from finders.find_pairs_cluster import Cluster
    from misc_funs import labels_to_could_be_pairs, plotqpairs

    fp = FindPairs(2, 100)
    pop = fp.population

    ca = Cluster.fit_kmeans_with_population(pop)
    labels = Cluster.recalc_centroids(ca, pop)

    assert np.all(np.unique(labels, return_counts=True)[1] == 2)

    qpairs = labels_to_could_be_pairs(labels, pop)
    plotqpairs(qpairs)

    assert np.all([np.all([m in fp.population for m in k]) for k in qpairs])
    assert np.all(np.array([len(k) for k in qpairs]) == 2)

    # assert fp.cluster()


def test_run():
    from finders.find_pairs_cluster import Cluster

    fp = FindPairs(2, 10)
    ca = Cluster(fp)
    ca.run()

    pass


def test_calc_nearest_neighbour():
    dims = 2
    size = 10

    fp = FindPairs(dims, size)

    nn = Cluster.calc_nearestneighbour(fp.population)

    assert np.shape(nn) == (size, dims)
