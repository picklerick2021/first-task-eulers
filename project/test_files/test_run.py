import os.path
import sys

import run

sys.path.append('../finders')

import pytest
import population


def test_pass():
    pass


def test_run_help(capsys):
    sys.argv = ['', '--help']
    import run
    run.init_name_is_main()
    captured = capsys.readouterr()
    assert '--help' in captured.out
    assert '--finder' in captured.out
    assert '--dims' in captured.out
    assert '--size' in captured.out
    assert '--seed' in captured.out
    assert '--max_runtime' in captured.out
    assert '--report' in captured.out
    assert '--custom' in captured.out


def check_finder_output(captured: str, calling: str):
    assert 'Using seed' in captured
    assert 'Initializing the population of size' in captured
    assert f'Calling {calling}' in captured
    assert 'The pairs are valid!' in captured
    assert 'Sum of all pair distances' in captured


def test_run_Dual(capsys):
    sys.argv = ['', '--finder', 'Dual']
    import run
    run.init_name_is_main()
    captured = capsys.readouterr()
    check_finder_output(captured.out, 'Dual')


def test_run_Custom(capsys):
    sys.argv = ['', '--finder', 'Custom', '--custom', 'CustomFinder.py']
    import run
    run.init_name_is_main()
    captured = capsys.readouterr()
    check_finder_output(captured.out, 'Custom')


@pytest.mark.xfail(raises=ModuleNotFoundError)
def test_run_CustomNotThere(capsys):
    filename = 'CustomFinderNotThere'
    sys.argv = ['', '--finder', 'Custom', '--custom', filename]
    import run
    try:
        run.init_name_is_main()
        captured = capsys.readouterr()
        assert False
    except SystemExit:
        captured = capsys.readouterr()
        assert f'Custom finder {filename} not found!' in captured.out
        assert True


def test_run_Dual_report(capsys):
    filename: str = os.path.join('reports', 'delete_test_report.ipynb')
    sys.argv = ['', '--finder', 'Dual', '--report', filename]
    import run
    run.init_name_is_main()
    captured = capsys.readouterr()
    assert 'Using seed' in captured.out
    assert 'Initializing the population of size' in captured.out
    assert f'Saved report to {filename}.' in captured.out
    assert os.path.isfile(filename)
    os.remove(filename)
