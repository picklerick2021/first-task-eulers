import sys
sys.path.append('../finders')

from find_pairs import FindPairs
import numpy as np

def test_label2pairs():
    from misc_funs import labels_to_could_be_pairs

    fp = FindPairs(2, 100)
    qpairs = labels_to_could_be_pairs(np.random.choice(int(len(fp.population) / 2), len(fp.population)), pop=fp.population)
    assert np.all([np.all([m in fp.population for m in k]) for k in qpairs])


def test_NearestNeighbour():
    from finders.find_pairs_nearest_neighbour import NearestNeighbour
    from find_pairs import FindPairs

    fp = FindPairs(2, 10)

    nearestneighbour = NearestNeighbour(fp)
    nearestneighbour.run()

    pass

def test_qpairs2index():
    from misc_funs import could_be_pairs_to_id

    fp = FindPairs(2, 100)
    qpairs = [list(fp.population[np.random.choice(len(fp.population), int(np.random.random() * 4 + 2))]) for k in
              range(int(len(fp.population) / 2))]
    qpairsID = could_be_pairs_to_id(qpairs, fp.population)