import numpy as np


def labels_to_could_be_pairs(labels, pop):
    qpairs = [[] for k in range(max(labels) + 1)]

    for i, k in enumerate(labels):
        qpairs[k].append(pop[i])

    return qpairs


def could_be_pairs_to_id(qpairs, pop):
    nqpairsq = np.array(qpairs)
    nqpairsqi = np.array([[np.where(k[0] == pop)[0][0], np.where(k[1] == pop)[0][0]] for k in nqpairsq])

    return nqpairsqi


def plotqpairs(qpairs):
    import matplotlib.pyplot as plt
    for k in qpairs:
        k = np.transpose(k)
        plt.plot(k[0], k[1])

    plt.show()
